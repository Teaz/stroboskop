# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://Teaz@bitbucket.org/Teaz/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/Teaz/stroboskop/commits/959a3edf24c832a6d42b166e617c6c2ab1a43d1b

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Teaz/stroboskop/commits/17f539b8aa6d1023b0fc965c038fbf60aa1efd0e

Naloga 6.3.2:
https://bitbucket.org/Teaz/stroboskop/commits/e3376744a5b41eaae7e04f7144ae2537e4499c00

Naloga 6.3.3:
https://bitbucket.org/Teaz/stroboskop/commits/ca6b870424defc776dda726bed31d1c7f817aed5

Naloga 6.3.4:
https://bitbucket.org/Teaz/stroboskop/commits/142c53bb549c4a69d0cf8242acfc08bb99572382

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Teaz/stroboskop/commits/e705893cee13b4d9b8ce885195617f3dd7f243f8

Naloga 6.4.2:
https://bitbucket.org/Teaz/stroboskop/commits/08ee56478e2cf7e46fbb70fe712c87198c67c45d

Naloga 6.4.3:
https://bitbucket.org/Teaz/stroboskop/commits/72d5512f7e6f121474a458c5b0103dfe6404d9c9

Naloga 6.4.4:
https://bitbucket.org/Teaz/stroboskop/commits/fe5ddd8d40462d87749013e37f15f2546ca91b3c